const FlightReducer = (state = {}, action) => {
    switch (action.type) {
      case 'FARE_UPDATE':
        return {
          ...state,
          fareFilter: action.fare,
        }
      case 'SEARCH_RESULT':
        return {
            ...state,
           searchResult: action.searchResult,
        }
      case 'SHOW_RESULT':
        return {
            ...state,
            showResult: action.showResult,
        }
      default:
        return state
    }
  }
  
  export default FlightReducer;