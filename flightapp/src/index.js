import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux'
import RootComponent from './components/RootComponent';
import FlightReducer from "../src/reducer/FlightReducer";
import "../node_modules/bootstrap/scss/bootstrap.scss";
import './index.css';
require("bootstrap");

const store = createStore(FlightReducer);

ReactDOM.render(
<Provider store={store}>
    <RootComponent />
</Provider>, document.getElementById('root'));
