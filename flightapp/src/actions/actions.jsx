export const fareUpdate = (fare) => ({
    type: 'FARE_UPDATE',
    fare,
  });
  
  export const searchResult = (searchResult) => ({
    type: 'SEARCH_RESULT',
    searchResult
  });

  export const showResult = (showResult) => ({
    type: 'SHOW_RESULT',
    showResult
  });
  