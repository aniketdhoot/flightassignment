import React, { Component } from 'react';
import { Card } from 'react-bootstrap';

const JourneyPath = (props) => {
    return (
        <div>
            <Card bg="light" border="white" style={{ width: '100%' }} className="cardContainer">
                <Card.Body>
                    <Card.Title className="margin-bottom-zero">{props.journeyPathData.origin.toUpperCase()} > {props.journeyPathData.origin.toUpperCase()}</Card.Title>
                </Card.Body>
            </Card>
        </div>
    );
};

export default JourneyPath;
