import React from 'react';
import { Card, Container, Row } from 'react-bootstrap';

const SearchResult = (props) => {
    const flightItem = props.resultDetails;
    return (
        <Container>
            <Row border="primary" className="margin-10">
                <Card bg="info" text="white" border="white" style={{ width: '100%' }} className="cardContainer">
                    <Card.Body>
                    <Card.Title>{flightItem.fare}</Card.Title>
                    <Card.Text className="margin-bottom-zero">{flightItem.flight_id}</Card.Text>
                    <Card.Text className="margin-bottom-zero">{flightItem.source_code} > {flightItem.destination_code}</Card.Text>
                    <Card.Text className="margin-bottom-zero">{flightItem.departs_at}</Card.Text>
                    <Card.Text className="margin-bottom-zero">{flightItem.arrives_at}</Card.Text>
                    </Card.Body>
                </Card>
            </Row>
        </Container>
    );
};

export default SearchResult;
