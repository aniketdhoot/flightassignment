import React from 'react';
import JourneyPath from "./JourneyPath";
import SearchResult from "./SearchResult";
import { connect } from 'react-redux';
const flightData = require('../../mockJson/flights');

const SearchSectionContainer = (props) => {
    const SearchResultFilter = () => {
        const {origin, destination} = props.resultItems;
        if(origin && destination) {
            return flightData.map((item, index) => {
                if(item.source.toLowerCase() === origin.toLowerCase() && item.destination.toLowerCase() === destination.toLowerCase()){
                    if(props.fareFilter) {
                        let fare = parseInt(item.fare.match(/([0-9])\w+/g), 10);
                        if(fare >= props.fareFilter.min && fare <= props.fareFilter.max){
                            return <SearchResult resultDetails={item}/>
                        }
                    } else {
                        return <SearchResult resultDetails={item}/>
                    }
                }
            });
        } else {
            alert('Please enter journey details');
        }
    };
    return (
        <div>
            <JourneyPath journeyPathData={props.resultItems}/>
            {SearchResultFilter()}
        </div>
    );
};

function mapStateToProps(state) {
    return {
        resultItems: state.searchResult,
        fareFilter: state.fareFilter
    };
}

export default connect(mapStateToProps)(SearchSectionContainer);