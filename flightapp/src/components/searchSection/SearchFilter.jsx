import React, {useState} from 'react';
import Slider from "@material-ui/core/Slider";
import { Container,Row } from 'react-bootstrap';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as searchActions from '../../actions/actions';

const SearchFilter = (props) => {
    const [value, setValue] = useState([0, 20000]);
    const handleChange = (event, newValue) => {
        setValue(newValue);
        props.searchActions.fareUpdate({
            'min': newValue[0],
            'max': newValue[1]
        });
    };
    return (
        <Container className="margin-top-20">
            <Row>
                <h4>Price Filter</h4>
            </Row>
            <Row>
                <Slider
                    value={value}
                    onChange={handleChange}
                    valueLabelDisplay="auto"
                    min={0}
                    max={20000}
                    aria-labelledby="range-slider"
                />
            </Row>
            <Row>
                <div>Rs {value[0]} to {value[1]}</div>
            </Row>
        </Container>
    );
}

function mapStateToProps(state) {
    return {
        resultItems: state.searchResult
    };
}
    
function mapDispatchToProps(dispatch) {
    return {
        searchActions : bindActionCreators(searchActions, dispatch)
    }
}
    
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchFilter);

