import React from 'react';
import SearchForm from "./SearchForm";
import SearchFilter from "./SearchFilter";

const SearchSectionContainer = (props) => {
    return (
        <div>
            <SearchForm />
            <SearchFilter />
        </div>
    );
};

export default SearchSectionContainer;