import "../../css/app.scss";
import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import Calendar from 'react-calendar';
import { connect } from 'react-redux';
import $ from "jquery";
import {bindActionCreators} from 'redux';
import * as searchActions from '../../actions/actions';


const SearchForm = (props) => {
    const [validated, setValidated] = useState(false);
    let searchDetails = {};
    function handleClick(e) {
        let calendar = document.querySelector('.calendar');
        calendar.style.display === 'block' ? calendar.style.display = 'none': calendar.style.display = 'block';
    };
   
    function handleSubmit(e) {
        e.preventDefault();
        e.stopPropagation();
        const form = $(e.currentTarget).get(0);
        setValidated(true);
		if (form.checkValidity()) {
            Object.keys(searchDetails).length && props.searchActions.searchResult(searchDetails);
            props.searchActions.showResult(true);
        }
    };
    function handleChange(e) {
        Object.assign(searchDetails , {
            [e.currentTarget.id] : e.currentTarget.value
        });
    };
    
    return (
            <Form validated={validated} onSubmit={handleSubmit} noValidate>
                <Form.Group controlId="origin">
                    <Form.Label>Enter Origin City</Form.Label>
                    <Form.Control required type="input" placeholder="Enter Origin City" onChange={e => handleChange(e)}/>
                    <Form.Control.Feedback type="invalid">Please enter source of journey</Form.Control.Feedback>
                </Form.Group>

                <Form.Group controlId="destination">
                    <Form.Label>Enter Destination City</Form.Label>
                    <Form.Control required type="input" placeholder="Enter Destination City" onChange={e => handleChange(e)}/>
                    <Form.Control.Feedback type="invalid">Please enter destination of journey</Form.Control.Feedback>
                </Form.Group>

                <Form.Group controlId="selectDate">
                    <Form.Label>Please select Date</Form.Label>
                    <Form.Control type="password" placeholder="Please Select Journey Date" onClick={(e) => handleClick(e)} onBlur={(e) => handleClick(e)}/>
                    <Form.Control.Feedback type="invalid">Please enter date of journey</Form.Control.Feedback>
                    <div className="calendar" style={{'display': 'none'}}>
                        <Calendar/>
                    </div>
                </Form.Group>

                <Form.Group controlId="passengers">
                    <Form.Label>Passengers</Form.Label>
                    <Form.Control required as="select" onClick={(e) => handleChange(e)}>
                    <Form.Control.Feedback type="invalid">Please select number of passengers</Form.Control.Feedback>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </Form.Control>
                </Form.Group>
                
                <Button variant="primary" type="submit">
                    Search
                </Button> 
            </Form>
        )
     };

    function mapStateToProps(state) {
        return state;
    }
    
    function mapDispatchToProps(dispatch) {
        return {
            searchActions : bindActionCreators(searchActions, dispatch)
        }
    }
    
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchForm);
