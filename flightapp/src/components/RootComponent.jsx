import React, { Component } from 'react';
import SearchSectionContainer from './searchSection/SearchSectionContainer';
import ResultSectionContainer from './resultSection/ResultSectionContainer';
import { Container, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';

class RootComponent extends Component {
    render() {
        return (
            <Container className = "appContainer">
                <h1>Flight Search Engine</h1>
                <Row className="justify-content-md-center">
                    <Col>
                        <SearchSectionContainer />
                    </Col>
                    <Col>
                        {this.props.showResult && <ResultSectionContainer />}
                    </Col>
                </Row>
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {showResult: state.showResult};
}

export default connect(mapStateToProps)(RootComponent);
